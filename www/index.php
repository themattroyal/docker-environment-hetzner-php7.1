<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>LAMP STACK | Hetzner</title>
        <link rel="stylesheet" href="/assets/css/bulma.min.css">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/trianglify/1.0.1/trianglify.min.js"></script>

          <script>
          (function() {
            document.addEventListener('DOMContentLoaded', function() {

            var pattern = Trianglify({
              cell_size: 75,
              variance: 0.35,
              seed: 'digitalhaha.co.za',
              x_colors: ['D3D2C7', '#6EC0C5', '#133DC6'],
              y_colors: 'random',
              color_space: 'rgb',
              color_function: false,
              width: window.innerWidth,
              height: window.innerHeight,
              color_function: null
            });

            window.onresize = function(){ location.reload(); }
            window.resize = function(){ location.reload(); }
            document.getElementById('bg').appendChild(pattern.canvas());
            });
          }());
          </script>

          <style type="text/css">
            
            section#bg {
                background: transparent;
            }

            #bg canvas {
                position: fixed;
                z-index: -1;
                height: 75vh;
                width: 100%;
            }

            section.section {
                background: #fff;
            }

          </style>
    </head>
    <body>
        <section id="bg" class="hero is-medium is-info is-bold">
            <div class="hero-body">
                <div class="container has-text-centered">
                    <h1 class="title">
                        HETZNER STACK
                    </h1>
                    <h2 class="subtitle">
                        Local development environment based on Hetzner
                    </h2>
                </div>
            </div>
        </section>
        <section class="section">
            <div class="container">
                <div class="columns">
                    <div class="column">
                        <h3 class="title is-3 has-text-centered">Environment</h3>
                        <hr>
                        <div class="content">
                            <ul>
                                <li><?= apache_get_version(); ?></li>
                                <li>PHP <?= phpversion(); ?></li>
                                <li>
                                    <?php
                                    $link = mysqli_connect("mysql", "root", "tiger", null);

/* check connection */
                                    if (mysqli_connect_errno()) {
                                        printf("MySQL connecttion failed: %s", mysqli_connect_error());
                                    } else {
                                        /* print server version */
                                        printf("MySQL Server %s", mysqli_get_server_info($link));
                                    }
                                    /* close connection */
                                    mysqli_close($link);
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="column">
                        <h3 class="title is-3 has-text-centered">Quick Links</h3>
                        <hr>
                        <div class="content">
                            <ul>
                                <li><a href="http://localhost:8088/websites" target="_blank">Websites</a></li>
                                <li><a href="http://localhost:8088/phpinfo.php" target="_blank">phpinfo()</a></li>
                                <li><a href="http://localhost:8080" target="_blank">phpMyAdmin</a></li>
                                <li><a href="http://localhost:8088/test_db.php" target="_blank">Test DB Connection</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>